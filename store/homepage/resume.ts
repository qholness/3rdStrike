export interface jobInterface {
    title: string,
    company: string,
    website: string,
    startDate: string,
    endDate: string
}


export interface educationInterface {
    institution: string,
    graduationYear: number,
    major: string,
    website: string
}


export interface skillInterface {
    name: string,
    proficiency: string,
    yearsOfExperience: number
}


function getYearsOfExperience(startYear: number) {
    let today = new Date();
    return today.getFullYear() - startYear;
}


export const skills = [
    {
        name: "Web Development",
        proficiency: "Great",
        yearsOfExperience: getYearsOfExperience(2015)
    } as skillInterface,
    {
        name: "Python",
        proficiency: "Great",
        yearsOfExperience: getYearsOfExperience(2014)
    } as skillInterface,
    {
        name: "Java",
        proficiency: "Good",
        yearsOfExperience: getYearsOfExperience(2017)
    } as skillInterface,
    {
        name: "Vue.js",
        proficiency: "Good",
        yearsOfExperience: getYearsOfExperience(2017)
    } as skillInterface,
]


export const education = [
    {
        institution: "Depaul University",
        school: "Driehaus College of Business",
        website: "https://business.depaul.edu",
        graduationYear: 2015,
        major: "Economics and Policy Analysis"
    } as educationInterface,
    {
        institution: "University of North Carolina at Charlotte",
        school: "Belk College of Business",
        website: "",
        graduationYear: 2015,
        major: "Finance"
    } as educationInterface,

]



export const jobs = [
    {
        title: "Data Scientist",
        company: "USAA",
        website: "usaa.com",
        startDate: "2020.9",
        endDate: "",
        
    } as jobInterface,
    {
        title: "Software Engineer",
        company: "USAA",
        website: "usaa.com",
        startDate: "2019.7",
        endDate: "2020.9",
    } as jobInterface,
    {
        title: "Jr. Software Engineer",
        company: "MarketDial",
        website: "marketdial.com",
        startDate: "2017.12",
        endDate: "2019.6",
    } as jobInterface,
    {
        title: "ETL Engineer",
        company: "Briostack",
        website: "briostack.com",
        startDate: "2017.1",
        endDate: "2017.12",
    } as jobInterface,
    {
        title: "Statistician",
        company: "Northwestern",
        website: "",
        startDate: "2015.10",
        endDate: "2017.1",
    } as jobInterface,
    {
        title: "Budget Analyst",
        company: "City of Chicago",
        website: "",
        startDate: "2014.10",
        endDate: "2015.1",
    } as jobInterface,
]