import { Character } from "./trainer/interfaces";


const characters: Record<string, Character> = {
  Akuma: { name: "Akuma", tier: 1 },
  Alex: { name: "Alex", tier: 3 },
  Chun: { name: "Chun", tier: 0 },
  Dudley: { name: "Dudley", tier: 1 },
  Elena: { name: "Elena", tier: 2 },
  Hugo: { name: "Hugo", tier: 3 },
  Ibuki: { name: "Ibuki", tier: 2 },
  Ken: { name: "Ken", tier: 0 },
  Makoto: { name: "Makoto", tier: 0 },
  Necro: { name: "Necro", tier: 3 },
  Oro: { name: "Oro", tier: 3 },
  Q: { name: "Q", tier: 4 },
  Remy: { name: "Remy", tier: 3 },
  Ryu: { name: "Ryu", tier: 2 },
  Sean: { name: "Sean", tier: 4 },
  Twelve: { name: "Twelve", tier: 4 },
  Urien: { name: "Urien", tier: 1 },
  Yang: { name: "Yang", tier: 1 },
  Yun: { name: "Yun", tier: 0 }
}


export default characters;