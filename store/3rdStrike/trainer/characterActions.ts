import akumaActions from "./characters/akumaActions";
import alexActions from "./characters/alexActions";
import chunActions from "./characters/chunActions";
import dudleyActions from "./characters/dudleyActions";
import elenaActions from "./characters/elenaActions";
import hugoActions from "./characters/hugoActions";
import ibukiActions from "./characters/ibukiActions";
import kenActions from "./characters/kenActions";
import makotoActions from "./characters/makotoActions";
import necroActions from "./characters/necroActions";
import oroActions from "./characters/oroActions";
import qActions from "./characters/qActions";
import remyActions from "./characters/remyActions";
import ryuActions from "./characters/ryuActions";
import seanActions from "./characters/seanActions";
import urienActions from "./characters/urienActions";
import yangActions from "./characters/yangActions";
import yunActions from "./characters/yunActions";
import { Action } from "./interfaces";


const characterActions: Array<Action> = []
const actionSet: Array < Array < Action >> = [
  chunActions,
  akumaActions,
  alexActions,
  dudleyActions,
  elenaActions,
  hugoActions,
  ibukiActions,
  kenActions,
  makotoActions,
  necroActions,
  oroActions,
  qActions,
  remyActions,
  ryuActions,
  seanActions,
  urienActions,
  yangActions,
  yunActions
]

actionSet.forEach(set_ => {
  set_.forEach(act_ => {
    characterActions.push(act_)
  })
})

export default characterActions;