export interface Action {
  id: string,
  character: Character,
  images: Array<string>,
  readable: string,
  startUp: number,
  active: number,
  // recovery: number
  punishWindow: number,
}


export interface Result {
  frames: number,
  responseTime: number,
  frameDiff: number,
  teched: boolean,
  attack: string,
  defense: string
}


export interface Character {
  name: string
  tier: number
}

export const throwAction: any = {
  startUp: 2,
  active: 1,
  punishWindow: 21
}

export const throwTechOffense: any = {
  startUp: 2,
  active: 1,
  // TODO: Get a good number for throws.
  punishWindow: 7
}

export const throwTechDefense: any = {
  startUp: 0,
  active: 1,
  punishWindow: 0
}