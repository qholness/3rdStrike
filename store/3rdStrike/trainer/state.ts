import characterActions from "./characterActions";
import { Action } from "./interfaces";


export interface TrainerState {
  ready: boolean
  started: boolean
  attack: Action
  defense: Action
}

// INFO: Default state
let trainerState: TrainerState = {
  ready: false,
  started: false,
  attack: characterActions[0],
  defense: characterActions[0]
}


export default trainerState;