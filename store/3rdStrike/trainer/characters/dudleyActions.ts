import { Action } from "../interfaces";
import characters from "../../characters";
import { generateThrows } from "./utils";



const dudleyActions: Action[] = []

generateThrows(characters.Dudley).forEach(throw_ => {
  dudleyActions.push(throw_)
})

export default dudleyActions;