import characters from "../../characters";
import { Action } from "../interfaces";
import { generateThrows } from "./utils";


const chunActions: Action[] = [
  {
    id: characters.Chun.name + "LowForward",
    character: characters.Chun,
    images: [characters.Chun.name + "LowForward.gif", characters.Chun.name + "Idle.gif"],
    readable: "Low Forward",
    startUp: 6,
    active: 2,
    punishWindow: 14
  },
  {
    id: characters.Chun.name + "BackFierce",
    character: characters.Chun,
    images: [characters.Chun.name + "BackFierce.gif", characters.Chun.name + "Idle.gif"],
    readable: "Back Fierce",
    startUp: 7,
    active: 9,
    punishWindow: 10
  },
  {
    id: characters.Chun.name + "CrouchJab",
    character: characters.Chun,
    images: [characters.Chun.name + "CrouchJab.gif", characters.Chun.name + "Idle.gif"],
    readable: "Crouch Jab",
    startUp: 2,
    active: 4,
    punishWindow: 5
  }
]


generateThrows(characters.Chun).forEach(throw_ => {
  chunActions.push(throw_)
})

export default chunActions;