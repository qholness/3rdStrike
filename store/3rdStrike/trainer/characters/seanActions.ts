import { Action } from "../interfaces";
import characters from "../../characters";
import { generateThrows } from "./utils";



const seanActions: Action[] = []

generateThrows(characters.Sean).forEach(throw_ => {
  seanActions.push(throw_)
})

export default seanActions;