import { Action } from "../interfaces";
import characters from "../../characters";
import { generateThrows } from "./utils";



const kenActions: Action[] = [
    {
    id: characters.Ken.name + "LowForward",
    character: characters.Ken,
    images: [characters.Ken.name + "LowForward.gif", characters.Ken.name + "Idle.gif"],
    readable: "Low Forward",
    startUp: 6,
    active: 5,
    punishWindow: 17
  },
  {
    id: characters.Ken.name + "Srk",
    character: characters.Ken,
    images: [characters.Ken.name + "Srk.gif", characters.Ken.name + "Idle.gif"],
    readable: "Srk (Jab)",
    startUp: 2,
    active: 8,
    punishWindow: 26
  }
]

generateThrows(characters.Ken).forEach(throw_ => {
  kenActions.push(throw_)
})

export default kenActions;