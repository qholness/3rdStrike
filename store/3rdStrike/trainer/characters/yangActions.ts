import { Action } from "../interfaces";
import characters from "../../characters";
import { generateThrows } from "./utils";



const yangActions: Action[] = []

generateThrows(characters.Yang).forEach(throw_ => {
  yangActions.push(throw_)
})

export default yangActions;