import { Action } from "../interfaces";
import characters from "../../characters";
import { generateThrows } from "./utils";



const yunActions: Action[] = []

generateThrows(characters.Yun).forEach(throw_ => {
  yunActions.push(throw_)
})

export default yunActions;