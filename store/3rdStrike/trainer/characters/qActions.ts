import { Action } from "../interfaces";
import characters from "../../characters";
import { generateThrows } from "./utils";



const qActions: Action[] = []

generateThrows(characters.Q).forEach(throw_ => {
  qActions.push(throw_)
})

export default qActions;