import { Action } from "../interfaces";
import characters from "../../characters";
import { generateThrows } from "./utils";



const akumaActions: Action[] = [
  { 
    id: characters.Akuma.name + "TatsuLightBlocked",
    character: characters.Akuma,
    images: [characters.Akuma.name + "Tatsu.gif", characters.Akuma.name + "Idle.gif"],
    readable: "Tatsu (Light) (Blocked)",
    startUp: 11,
    active: 4,
    punishWindow: 12
  },
  { 
    id: characters.Akuma.name + "TatsuLightWhiffed",
    character: characters.Akuma,
    images: [characters.Akuma.name + "Tatsu.gif", characters.Akuma.name + "Idle.gif"],
    readable: "Tatsu (Light) (Whiffed)",
    startUp: 11,
    active: 4,
    punishWindow: 17
  },
  { 
    id: characters.Akuma.name + "TatsuRoundhouseBlocked",
    character: characters.Akuma,
    images: [characters.Akuma.name + "Tatsu.gif", characters.Akuma.name + "Idle.gif"],
    readable: "Tatsu (Roundhouse) (Blocked)",
    startUp: 2,
    active: 18,
    punishWindow: 5
  },
  { 
    id: characters.Akuma.name + "TatsuRoundhouseWhiffed",
    character: characters.Akuma,
    images: [characters.Akuma.name + "Tatsu.gif", characters.Akuma.name + "Idle.gif"],
    readable: "Tatsu (Roundhouse) (Whiffed)",
    startUp: 2,
    active: 18,
    punishWindow: 10
  }
]

generateThrows(characters.Akuma).forEach(throw_ => {
  akumaActions.push(throw_)
})

export default akumaActions;