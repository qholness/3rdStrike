import { Action } from "../interfaces";
import characters from "../../characters";
import { generateThrows } from "./utils";



const necroActions: Action[] = []

generateThrows(characters.Necro).forEach(throw_ => {
  necroActions.push(throw_)
})

export default necroActions;