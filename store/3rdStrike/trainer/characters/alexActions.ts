import { Action } from "../interfaces";
import characters from "../../characters";
import { generateThrows } from "./utils";



const alexActions: Action[] = []

generateThrows(characters.Alex).forEach(throw_ => {
  alexActions.push(throw_)
})

export default alexActions;