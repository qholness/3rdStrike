import { Action } from "../interfaces";
import characters from "../../characters";
import { generateThrows } from "./utils";



const ibukiActions: Action[] = []

generateThrows(characters.Ibuki).forEach(throw_ => {
  ibukiActions.push(throw_)
})

export default ibukiActions;