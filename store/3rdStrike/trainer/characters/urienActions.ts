import { Action } from "../interfaces";
import characters from "../../characters";
import { generateThrows } from "./utils";



const urienActions: Action[] = []

generateThrows(characters.Urien).forEach(throw_ => {
  urienActions.push(throw_)
})

export default urienActions;