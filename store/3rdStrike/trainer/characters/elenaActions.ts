import { Action } from "../interfaces";
import characters from "../../characters";
import { generateThrows } from "./utils";



const elenaActions: Action[] = []

generateThrows(characters.Elena).forEach(throw_ => {
  elenaActions.push(throw_)
})

export default elenaActions;