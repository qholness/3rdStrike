import { Action } from "../interfaces";
import characters from "../../characters";
import { generateThrows } from "./utils";



const ryuActions: Action[] = []

generateThrows(characters.Ryu).forEach(throw_ => {
  ryuActions.push(throw_)
})

export default ryuActions;