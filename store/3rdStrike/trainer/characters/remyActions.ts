import { Action } from "../interfaces";
import characters from "../../characters";
import { generateThrows } from "./utils";



const remyActions: Action[] = []

generateThrows(characters.Remy).forEach(throw_ => {
  remyActions.push(throw_)
})

export default remyActions;