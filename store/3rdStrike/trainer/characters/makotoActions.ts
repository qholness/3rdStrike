import { Action } from "../interfaces";
import characters from "../../characters";
import { generateThrows } from "./utils";



const makotoActions: Action[] = [
  { 
    id: characters.Makoto.name + "DashForward",
    character: characters.Makoto,
    images: [characters.Makoto.name + "DashForward.gif", characters.Makoto.name + "Idle.gif"],
    readable: "Dash (Forward)",
    startUp: 2,
    active: 8,
    punishWindow: 14
  },
  { 
    id: characters.Makoto.name + "DashBack",
    character: characters.Makoto,
    images: [characters.Makoto.name + "DashBackward.gif", characters.Makoto.name + "Idle.gif"],
    readable: "Dash (Backward)",
    startUp: 1,
    active: 4,
    punishWindow: 6 + 4
  },
  {
    id: characters.Makoto.name + "Hayate",
    character: characters.Makoto,
    images: [characters.Makoto.name + "Hayate.gif", characters.Makoto.name + "Idle.gif"],
    readable: "Hayate (Jab)",
    startUp: 6,
    active: 6,
    punishWindow: 12
  },
  {
    id: characters.Makoto.name + "Hayate",
    character: characters.Makoto,
    images: [characters.Makoto.name + "Hayate.gif", characters.Makoto.name + "Idle.gif"],
    readable: "Hayate (Strong)",
    startUp: 11,
    active: 6,
    punishWindow: 12
  },
  {
    id: characters.Makoto.name + "Hayate",
    character: characters.Makoto,
    images: [characters.Makoto.name + "Hayate.gif", characters.Makoto.name + "Idle.gif"],
    readable: "Hayate (Fierce)",
    startUp: 9,
    active: 6,
    punishWindow: 12
  },
]

generateThrows(characters.Makoto).forEach(throw_ => {
  makotoActions.push(throw_)
})

export default makotoActions;