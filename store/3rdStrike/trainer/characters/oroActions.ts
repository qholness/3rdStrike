import { Action } from "../interfaces";
import characters from "../../characters";
import { generateThrows } from "./utils";



const oroActions: Action[] = []

generateThrows(characters.Oro).forEach(throw_ => {
  oroActions.push(throw_)
})

export default oroActions;