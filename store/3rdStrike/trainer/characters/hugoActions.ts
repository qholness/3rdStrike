import { Action } from "../interfaces";
import characters from "../../characters";
import { generateThrows } from "./utils";



const hugoActions: Action[] = []

generateThrows(characters.Hugo).forEach(throw_ => {
  hugoActions.push(throw_)
})

export default hugoActions;