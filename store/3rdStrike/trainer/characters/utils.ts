import { Action, Character, throwAction, throwTechDefense, throwTechOffense } from "../interfaces"


export function generateThrows(character: Character): Action[] {
  return [
    { 
      id: character.name + "Throw",
      character: character,
      images: [character.name + "Throw.gif", character.name + "Idle.gif"],
      readable: "Throw (Whiffed)",
      startUp: throwAction.startUp,
      active: throwAction.active,
      punishWindow: throwAction.punishWindow
    },
    { 
      id: character.name + "ThrowTechOffense",
      character: character,
      images: [character.name + "Throw.gif", character.name + "Idle.gif"],
      readable: "Throw (Offense)",
      // startUp + grabFrame
      startUp: throwTechOffense.startUp,
      active: throwTechOffense.active,
      punishWindow: throwTechOffense.punishWindow
    },
    { 
      id: character.name + "ThrowTechDefense",
      character: character,
      images: [character.name + "Throw.gif", character.name + "Idle.gif"],
      readable: "Tech Throw",
      // startUp + grabFrame
      startUp: throwTechDefense.startUp,
      active: throwTechDefense.active,
      punishWindow: throwTechDefense.punishWindow
    }
  ]
}
